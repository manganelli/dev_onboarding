# DEV Backoffice Onboarding

## Qual a moral desse repo?
Organizar todas informações relevantes para DEVs que estão começando no Backoffice.



## Configurar BitBucket
1. Solicitar acesso ao workspace [taglivrosti](https://bitbucket.org/taglivrosti)
2. Configurar [SSH key Bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)



## Configurar acesso ao Jira
1. Conseguir acessar sprint backlog no [Jira](https://taglivros.atlassian.net/secure/RapidBoard.jspa?rapidView=34&projectKey=BAC)



## Configurar acesso ao Trello (Suporte)
1. Solicitar acesso ao board [suporte-oef](https://trello.com/b/EydGDFmw/suporte-oef)



## Configurar acesso a AWS
1. Solicitar credenciais para acesso a [AWS TAG console](https://taglivros.signin.aws.amazon.com/console)
2. Instalar [AWS CLI](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-cliv2-linux.html)
3. Configurar [AWS CLI](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-config)



## Clonar repos && Start docker containers
1. Criar pasta **taglivrosti** no diretório home do seu usuário `cd ~/`. Esta pasta **deve** conter todos os repos da TAG.
2. Clonar repo de scripts `git clone git@bitbucket.org:taglivrosti/oef_scripts.git` 
3. Executar scripts `sudo ./oef_scripts/setup/backoffice/local/00_setup_local.sh`
4. Executar `ls` e verificar se todos os repos foram clonados
5. Executar `docker images` e `docker ps` para verificar se as imagens e containers foram criados



## Base de conhecimento
- [FAQ Suporte](https://docs.google.com/document/d/1RDtc1__wNXWZ5bXnaiZR1Y5yCs9juTeC-9W4pk7WmPU/edit#)
- [GDrive TI](https://drive.google.com/drive/folders/1gDdHq5M2vAs7hhLsurgS5ynyvgELTZma)
- [Como fazer queries no Django](https://drive.google.com/file/d/1Mj-hFBrugVwDuhBAIao6zFSwcN3ZA_is/view?usp=sharing)



## TODO
- Adicionar outras informações relevantes ;)



